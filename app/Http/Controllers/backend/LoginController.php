<?php

namespace App\Http\Controllers\backend;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    function GetLogin()
    {
        return view("backend.login.login");
    }

    function PostLogin(LoginRequest $r){
        $email=$r->input('email');
        $password=$r->input('password');
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            return redirect('admin');
        }else {
            return redirect()->back()->with("thongbao","Tài Khoản hoặc Mật Khẩu không chính xác!!")->withInput();
        }
    }
}
