<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Backend
Route::get('login','backend\LoginController@GetLogin')->middleware('CheckLogout');
Route::post('login','backend\LoginController@PostLogin');

Route::group(['prefix' => 'admin','middleware'=>'CheckLogin'], function () {
    Route::get('','backend\IndexController@GetIndex');
    Route::get('logout','backend\IndexController@Logout');
    //category
    Route::group(['prefix' => 'category'], function () {
        Route::get('/','backend\CategoryController@index');
        Route::get('{category}/edit','backend\CategoryController@edit');
        Route::post('/','backend\CategoryController@store');
    });

    //user
    Route::group(['prefix' => 'user'], function () {
        Route::get('/','backend\UserController@index');
        Route::get('create','backend\UserController@create');
        Route::post('/','backend\UserController@store');
        Route::get('{user}/edit','backend\UserController@edit');
    });


    //product
    Route::group(['prefix' => 'product'], function () {
        Route::get('/','backend\ProductController@index');
        Route::get('create','backend\ProductController@create');
        Route::post('/','backend\ProductController@store');
        Route::get('{product}/edit','backend\ProductController@edit');
    });

    //order
    Route::group(['prefix' => 'order'], function () {
        Route::get('','backend\OrderController@GetOrder');
        Route::get('detail','backend\OrderController@GetDetailOrder');
        Route::get('processed','backend\OrderController@GetProcessed');
    });

    
});